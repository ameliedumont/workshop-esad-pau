
var p = document.getElementsByTagName('p');

var cssPagedMedia = (function () {
    var style = document.createElement('style');
    document.head.appendChild(style);
    return function (rule) {
        style.innerHTML = rule;
    };
}());

cssPagedMedia.size = function (width, height) {
    cssPagedMedia('@page {size: ' + width + 'mm ' + height + 'mm; margin: 0 !important; padding: 0 !important}');
};


var w = 150;
var h = 90;

cssPagedMedia.size(w, h);


function changePageWidth() {
  var wid = document.getElementById('pageWidth');
  var page = document.getElementById('page_titre');
  var texte = document.getElementById('texte');
  w = wid.value;
  page.style.width = w.toString() + "mm" ;
  texte.style.width = w.toString() + "mm";
  cssPagedMedia.size(w, h);
}

function changePageHeight() {
  var hei = document.getElementById('pageHeight');
  var page = document.getElementById('page_titre');
  var texte = document.getElementById('texte');
  h = hei.value;
  page.style.height = (h-5).toString() + "mm";
  texte.style.height = (h-5).toString() + "mm";
  cssPagedMedia.size(w, h);
}

function changeAuthor() {
  let newText = document.getElementById('subText').value;
  let author = document.getElementById('p3');
  author.innerHTML = newText;
}

function changeYear() {
  let newText = document.getElementById('yearText').value;
  let year = document.getElementById('p2');
  year.innerHTML = newText;
}

function changeTitle() {
  let newText = document.getElementById('titleText').value;
  let title = document.getElementById('p4');
  title.innerHTML = newText;
}

function changeText() {
  let newText = document.getElementById('txt').value;
  let text = document.getElementById('backText');
  text.innerHTML = newText;
}

function sizeText() {
  let newText = document.getElementById('sizeText').value;
  let text = document.getElementById('backText');
  text.style.fontSize = newText.toString() + "pt";
}

function svgCircle() {
  let newText = document.getElementById('svg1').value;
  let circle1 = document.getElementById('svg01');
  let circle2 = document.getElementById('svg03');
  let circle3 = document.getElementById('svg04');
  console.log(newText)
  circle1.setAttributeNS(null, 'r', newText);
  circle2.setAttributeNS(null, 'r', newText/2);
  circle3.setAttributeNS(null, 'r', newText*2);
}

let polygonPoints = [220,10, 300,210, 170,250, 123,234]

function polygonMove1() {
  let newText = document.getElementById('svg2').value;
  let poly = document.getElementById('svg02');
  
  polygonPoints.splice(0, 1, newText);
  polygonPoints.splice(1, 1, newText);
  console.log(polygonPoints)
  poly.setAttributeNS(null, 'points', polygonPoints[0].toString() + "," + polygonPoints[1].toString() + " " + polygonPoints[2].toString() + "," + polygonPoints[3].toString() + " " + polygonPoints[4].toString() + "," + polygonPoints[5].toString() + " " + polygonPoints[6].toString() + "," + polygonPoints[7].toString());
}

function polygonMove2() {
  let newText = document.getElementById('svg3').value;
  let poly = document.getElementById('svg02');
  polygonPoints.splice(2, 1, newText);
  polygonPoints.splice(3, 1, newText);
  console.log(polygonPoints)
  poly.setAttributeNS(null, 'points', polygonPoints[0].toString() + "," + polygonPoints[1].toString() + " " + polygonPoints[2].toString() + "," + polygonPoints[3].toString() + " " + polygonPoints[4].toString() + "," + polygonPoints[5].toString() + " " + polygonPoints[6].toString() + "," + polygonPoints[7].toString());;
}

function polygonMove3() {
  let newText = document.getElementById('svg4').value;
  let poly = document.getElementById('svg02');
  polygonPoints.splice(4, 1, newText);
  polygonPoints.splice(5, 1, newText);
  console.log(polygonPoints)
  poly.setAttributeNS(null, 'points', polygonPoints[0].toString() + "," + polygonPoints[1].toString() + " " + polygonPoints[2].toString() + "," + polygonPoints[3].toString() + " " + polygonPoints[4].toString() + "," + polygonPoints[5].toString() + " " + polygonPoints[6].toString() + "," + polygonPoints[7].toString());;
}


