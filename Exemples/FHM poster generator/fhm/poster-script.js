function changeBgColor() {
  let backColor = document.getElementById('backColor');
  let newColor = document.getElementById('bColor').value;
  let title = document.getElementById('event01');
  backColor.style.backgroundColor = newColor;
  title.style.textShadow = "-10px -10px " + newColor;
}

function changeTitleColor() {
  let newColor = document.getElementById('tColor').value;
  let title = document.getElementById('event01');
  let subtitle = document.getElementById('subSubtitle');
  title.style.color = newColor;
  subtitle.style.backgroundColor = newColor;
}

function changeSubtitle() {
  let newText = document.getElementById('subText').value;
  let subtitle = document.getElementById('subSubtitle');
  subtitle.innerHTML = newText;
}

function changeSubtitleColor() {
  let newColor = document.getElementById('sColor').value;
  let subtitle = document.getElementById('subSubtitle');
  subtitle.style.color = newColor;
}

function hideImg() {
  let eventImg = document.getElementById('imgEvent');
  let check = document.getElementById('imgButton');
  let subtitle = document.getElementById('subtitle');
  if(check.checked) {
    eventImg.style.display = "none";
    subtitle.style.top = "28mm";
  } else {
    eventImg.style.display = "block";
    subtitle.style.top = "150mm";
  }
}

window.addEventListener('load', function() {
  document.getElementById('eventImg').addEventListener('change', function() {
      if (this.files && this.files[0]) {
          var img = document.getElementById('imgOfEvent');
          img.onload = () => {
              URL.revokeObjectURL(img.src);  // no longer needed, free memory
              //console.log(img.src)
          }
          img.src = URL.createObjectURL(this.files[0]); // set src to blob url
          console.log(img.src)
      }
  });
});
