var c = document.getElementById("post");
var ctx = c.getContext("2d");

var f = new FontFace('Authentic', 'url(lesbiennale/fonts/AUTHENTICSans-130.ttf)');
var artist_name = 'Artist name';
var artist_multiline = false;
var artist_color = "black";
var artist_stroke = "white";
var artist_l1 = [];
var artist_l2 = [];

var artwork_title = 'Title of artwork';
var title_color = "white";
var title_multiline = false;
var title_l1 = [];
var title_l2 = [];

var branch1 = 70;
var branch2 = 130;
var branch3 = 0;

var img_source = 'lesbiennale/img_to_replace.png';
var img_artwork = new Image();

f.load().then(function(font) {
  console.log('font ready');
  document.fonts.add(font);
});

ctx.beginPath();
ctx.rect(0, 0, 680, 680);
ctx.fill();
ctx.closePath();

function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min)
}

make_base();

function make_base(){
  ctx.fillStyle = 'black';
  ctx.rect(0, 0, 720, 720);
  ctx.fill();
  logo_lesbiennale = new Image();
  logo_lesbiennale.src = 'lesbiennale/logo_white_on_black.png';
  logo_lesbiennale.onload = function(){
    ctx.drawImage(logo_lesbiennale, 290, 13);
  }
  infos = new Image();
  infos.src = 'lesbiennale/infos.png';
  infos.onload = function(){
    ctx.drawImage(infos, 635, 30);
  }
  
  img_artwork.src = img_source;
  img_artwork.onload = function() {
    if(img_artwork.width >= img_artwork.height) {
      var coeff = img_artwork.width/540;
      if(coeff > 3.0) {
        ctx.drawImage(img_artwork, -15, 129, 620, img_artwork.height/(img_artwork.width/620));
      } else {
        ctx.drawImage(img_artwork, 65, 129, 540, img_artwork.height/coeff);
      }
      console.log(coeff);
    } else {
      var coeff = img_artwork.height/540;
      ctx.drawImage(img_artwork, 65, 129, img_artwork.width/coeff, 540);
      console.log(coeff);
    }
    
    ctx.font = '72px Authentic';
    ctx.strokeStyle = artist_stroke;
    ctx.lineWidth = 2;
    ctx.fillStyle = artist_color;
    if(artist_multiline && title_multiline) {
      ctx.fillText(artist_l1.join(' '), 30, 510);
      ctx.strokeText(artist_l1.join(' '), 30, 510);
      ctx.fillText(artist_l2.join(' '), 30, 575);
      ctx.strokeText(artist_l2.join(' '), 30, 575);
      ctx.fillStyle = title_color;
      ctx.font = '56px Authentic';
      ctx.fillText(title_l1.join(' '), 30, 635);
      ctx.fillText(title_l2.join(' '), 30, 688);
    } else if(artist_multiline && !title_multiline){
      ctx.fillText(artist_l1.join(' '), 30, 565);
      ctx.strokeText(artist_l1.join(' '), 30, 565);
      ctx.fillText(artist_l2.join(' '), 30, 630);
      ctx.strokeText(artist_l2.join(' '), 30, 630);
      ctx.fillStyle = title_color;
      ctx.font = '56px Authentic';
      ctx.fillText(artwork_title, 30, 688);
    } else if(title_multiline && !artist_multiline) {
      ctx.fillText(artist_name, 30, 575);
      ctx.strokeText(artist_name, 30, 575);
      ctx.fillStyle = title_color;
      ctx.font = '56px Authentic';
      ctx.fillText(title_l1.join(' '), 30, 635);
      ctx.fillText(title_l2.join(' '), 30, 688);
    } else {
      ctx.fillText(artist_name, 30, 630);
      ctx.strokeText(artist_name, 30, 630);
      ctx.fillStyle = title_color;
      ctx.font = '56px Authentic';
      ctx.fillText(artwork_title, 30, 688);
    }

    logo_elc = new Image();
    logo_elc.src = 'lesbiennale/elc.png';
    logo_elc.onload = function(){
      ctx.drawImage(logo_elc, 585, 510);
    }
    draw_asterisk();
    
  }
  
}

function draw_asterisk() {

  var grd1 = ctx.createLinearGradient(0, 0, 200, 0);
    grd1.addColorStop(0, "#00aa88");
    grd1.addColorStop(1, "#bc5fd3");
    ctx.fillStyle = grd1;
    ctx.translate(30, 110);
    ctx.rotate(branch3 * Math.PI / 180);
    ctx.fillRect(0, 0, 230, 45);   
    ctx.resetTransform();
    ctx.translate(120, 30);
    ctx.rotate(branch1 * Math.PI / 180);
    ctx.fillRect(0, 0, 230, 45);   
    ctx.resetTransform();
    ctx.translate(240, 80);
    ctx.rotate(branch2 * Math.PI / 180);
    ctx.fillRect(0, 0, 230, 45);   
    ctx.resetTransform();

}


function exportCanvas(){ 
  var mycanvas = document.getElementById("post");
  if(mycanvas && mycanvas.getContext) {
    var img = mycanvas.toDataURL("image/png;base64;");
    anchor = document.getElementById("download");
    anchor.href = img;
    anchor.innerHTML = "Download";
  }
}

function changeArtist() {
  var artist = document.getElementById('artist').value;
  artist_l1 = [];
  artist_l2 = [];
  if(artist.length > 15) {
    artist_multiline = true;
    var artist_words = (artist.split(' '));
    for(let i= 0; i < artist_words.length; i++) {
      if(artist_l1.join(' ').length < 8) {
        artist_l1.push(artist_words[i]);
      } else {
        artist_l2.push(artist_words[i]);
      }
    }
    console.log("artist1" + artist_l1);
    console.log("artist2" + artist_l2);
  } else {
    artist_multiline = false;
    artist_name = artist;
  }
  var color_option = document.getElementById('artist_color').value;
  if(color_option == 1) {
    artist_color = "black";
    artist_stroke = "white";
  } else {
    artist_color = "white";
    artist_stroke = "#bc5fd3";
  }
  make_base();
}

function changeTitle() {
  var title = document.getElementById('artwork').value;
  title_l1 = [];
  title_l2 = [];
  if(title.length > 22) {
    title_multiline = true;
    var title_words = (title.split(' '));
    for(let i= 0; i < title_words.length; i++) {
      if(title_l1.join(' ').length < 19) {
        title_l1.push(title_words[i]);
      } else {
        title_l2.push(title_words[i]);
      }
    }
    console.log("title1" + title_l1);
    console.log("title2" + title_l2);
  } else {
    title_multiline = false;
    artwork_title = title;
  }  
  var color_option = document.getElementById('title_color').value;
  if(color_option == 1) {
    title_color = "white";
  } else if(color_option == 2){
    title_color = "#bc5fd3";
  } else {
    title_color = "#00aa88";
  }
  make_base();
}

function changeBranch1() {
  ctx.fillStyle = "rgba(0, 0, 0, 1)";
  ctx.strokeStyle= "white";
  ctx.translate(120, 30);
  ctx.rotate(branch1 * Math.PI / 180);
  ctx.fillRect(0, 0, 230, 45);
  ctx.strokeRect(0, 0, 230, 45);
  ctx.resetTransform();
  branch1 = document.getElementById("branch1").value;
  draw_asterisk();
}

function changeBranch2() {
  ctx.fillStyle = "rgba(0, 0, 0, 1)";
  ctx.strokeStyle= "white";
  ctx.translate(240, 80);
  ctx.rotate(branch2 * Math.PI / 180);
  ctx.fillRect(0, 0, 230, 45); 
  ctx.strokeRect(0, 0, 230, 45);
  ctx.resetTransform();
  branch2 = document.getElementById("branch2").value;
  draw_asterisk();
}

function changeBranch3() {
  ctx.fillStyle = "rgba(0, 0, 0, 1)";
  ctx.strokeStyle= "white";
  ctx.translate(30, 110);
  ctx.rotate(branch3 * Math.PI / 180);
  ctx.fillRect(0, 0, 230, 45); 
  ctx.strokeRect(0, 0, 230, 45);
  ctx.resetTransform();
  branch3 = document.getElementById("branch3").value;
  draw_asterisk();
}

window.addEventListener('load', function() {
  document.getElementById('eventImg').addEventListener('change', function() {
      if (this.files && this.files[0]) {
          img_source = URL.createObjectURL(this.files[0]); 
          make_base();
      }
  });
});
