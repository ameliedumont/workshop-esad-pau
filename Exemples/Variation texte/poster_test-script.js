
var p = document.getElementsByTagName('p');


function changeTextSize() {
  var rangeslider1 = document.getElementById("textSize");
  var val1 = rangeslider1.value;
  for(var i = 0; i < p.length; i++) {
    p[i].style.fontSize = val1.toString() + "pt";
  }
}

function changeTextSkew() {
  var titre = document.getElementById('titre');
  var rangeslider2 = document.getElementById("textSkew");
  var val2 = rangeslider2.value;
  titre.style.transform = "skew(" + val2.toString() + "deg, " + val2.toString() + "deg)";
}

function rotateText() {
  var rangeslider3 = document.getElementById("textRotate");
  var val3 = rangeslider3.value;
  for(var i = 0; i < p.length; i++) {
    p[i].style.transform = "rotate(" + val3.toString() +"deg)";
  }
}

var clones = [];

function titleClone() {
  var original = document.getElementById('titre');
  var cloneN = original.cloneNode(true);
  clones.push(cloneN);
  var left = 3;
  var top = 3;
  for(var i = 0; i < clones.length; i++) {
    console.log(clones);
    clones[i].style.left = left.toString() + "%";
    clones[i].style.marginTop = top.toString() + "%";
    original.parentNode.appendChild(cloneN);
    if(i < 30) {
      left += 3;
      top += 3;
    } else if(i >= 30 && i < 60) {
      left -= 3;
      top += 3;
    } else {
      left += 3;
      top += 3;
    }
  }
  
}


